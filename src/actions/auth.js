import axios from 'axios';
import { getEndpoint } from '../utils/routes';
import history from '../utils/history';

export const AUTH_REQUEST = 'AUTH_REQUEST';
const authRequest = () => {
    return {
        type: AUTH_REQUEST
    }
}

export const AUTH_REQUEST_SUCCESS = 'AUTH_REQUEST_SUCCESS';
const authRequestSuccess = (data) => {
    return {
        type: AUTH_REQUEST_SUCCESS,
        data
    }
}

export const AUTH_REQUEST_FAILURE = 'AUTH_REQUEST_FAILURE';
const authRequestFailure = (msg) => {
    return {
        type: AUTH_REQUEST_FAILURE,
        msg
    }
}

export const authenticate = (email, password) => {
    return async dispatch => {
        dispatch(authRequest());
        try {
            const response = await axios.post(getEndpoint('auth', 'signin'), {
                email, password
            });
            if(response.status === 200) {
                dispatch(authRequestSuccess(response.data));
                history.push(`/chat`);
            }
        } catch(err) {
            dispatch(authRequestFailure(err.message));
        }
    }
}

export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
const logoutRequest = () => {
    return {
        type: LOGOUT_REQUEST
    }
}

export const logout = () => {
    return async dispatch => {
        dispatch(logoutRequest());
        history.push(`/`);
    }
}

export const SIGN_UP_REQUEST = 'SIGN_UP_REQUEST';
const signUpRequest = () => {
    return {
        type: SIGN_UP_REQUEST
    }
}

export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';
const signUpSuccess = (data) => {
    return {
        type: SIGN_UP_SUCCESS,
        data
    }
}

export const SIGN_UP_FAILURE = 'SIGN_UP_FAILURE';
const signUpFailure = (msg) => {
    return {
        type: SIGN_UP_FAILURE,
        msg
    }
}

export const signup = (data) => {
    return async dispatch => {
        dispatch(signUpRequest());
        try {
            const response = await axios.post(getEndpoint('auth', 'signup'), {
                ...data
            });
            if(response.status === 200) {
                dispatch(signUpSuccess(response.data));
                history.push('/chat');
            }
        } catch(err) {
            dispatch(signUpFailure(err.message));
        }
    }
}

import axios from 'axios';
import { getEndpoint } from '../utils/routes';

export const PRIVATE_CONVERSATIONS_REQUEST = 'PRIVATE_CONVERSATIONS_REQUEST';
const privateConversationsRequest = () => {
    return {
        type: PRIVATE_CONVERSATIONS_REQUEST
    }
}

export const PRIVATE_CONVERSATIONS_REQUEST_FAILURE = 'PRIVATE_CONVERSATIONS_REQUEST_FAILURE';
const privateConversationsRequestFailure = (error) => {
    return {
        type: PRIVATE_CONVERSATIONS_REQUEST_FAILURE,
        error
    }
}

export const PRIVATE_CONVERSATIONS_REQUEST_SUCCESS = 'PRIVATE_CONVERSATIONS_REQUEST_SUCCESS';
const privateConversationsRequestSuccess = (data) => {
    return {
        type: PRIVATE_CONVERSATIONS_REQUEST_SUCCESS,
        data
    }
}

export const fetchPrivateConversations = (socket, token) => {
    return async dispatch => {
        dispatch(privateConversationsRequest());
        try {
            const response = await axios({
                method: 'get',
                url: getEndpoint('conversations', 'private'),
                headers: {'token': token}
            });
            if(response.status === 200) {
                dispatch(privateConversationsRequestSuccess(response.data));
                if(response.data.conversations.length) {
                    socket.emit('subscribe-list', {
                        conversations: response.data.conversations,
                        token
                    });
                }
            }
        } catch(err) {
            dispatch(privateConversationsRequestFailure(err));
        }
    }
}

export const GROUP_CONVERSATIONS_REQUEST = 'GROUP_CONVERSATIONS_REQUEST';
const groupConversationsRequest = () => {
    return {
        type: GROUP_CONVERSATIONS_REQUEST
    }
}

export const GROUP_CONVERSATIONS_REQUEST_FAILURE = 'GROUP_CONVERSATIONS_REQUEST_FAILURE';
const groupConversationsRequestFailure = (error) => {
    return {
        type: GROUP_CONVERSATIONS_REQUEST_FAILURE,
        error
    }
}

export const GROUP_CONVERSATIONS_REQUEST_SUCCESS = 'GROUP_CONVERSATIONS_REQUEST_SUCCESS';
const groupConversationsRequestSuccess = (data) => {
    return {
        type: GROUP_CONVERSATIONS_REQUEST_SUCCESS,
        data
    }
}

export const fetchGroupConversations = (token) => {
    return async dispatch => {
        dispatch(groupConversationsRequest());
        try {
            const response = await axios({
                method: 'get',
                url: getEndpoint('conversations', 'group'),
                headers: {'token': token}
            });
            if(response.status === 200) {
                dispatch(groupConversationsRequestSuccess(response.data));
            }
        } catch(err) {
            dispatch(groupConversationsRequestFailure(err));
        }
    }
}

export const PRIVATE_CONVERSATION_REQUEST = 'PRIVATE_CONVERSATION_REQUEST';
const privateConversationRequest = () => {
    return {
        type: PRIVATE_CONVERSATION_REQUEST
    }
}

export const PRIVATE_CONVERSATION_REQUEST_FAILURE = 'PRIVATE_CONVERSATION_REQUEST_FAILURE';
const privateConversationRequestFailure = (error) => {
    return {
        type: PRIVATE_CONVERSATION_REQUEST_FAILURE,
        error
    }
}

export const PRIVATE_CONVERSATION_REQUEST_SUCCESS = 'PRIVATE_CONVERSATION_REQUEST_SUCCESS';
const privateConversationRequestSuccess = (data) => {
    return {
        type: PRIVATE_CONVERSATION_REQUEST_SUCCESS,
        data
    }
}

export const fetchPrivateConversation = (token, id) => {
    return async dispatch => {
        dispatch(privateConversationRequest());
        try {
            const response = await axios({
                method: 'get',
                url: `${getEndpoint('conversations', 'private')}/${id}`,
                headers: {'token': token}
            });
            if(response.status === 200) {
                dispatch(privateConversationRequestSuccess(response.data));
            }
        } catch(err) {
            dispatch(privateConversationRequestFailure(err));
        }
    }
}

export const GROUP_CONVERSATION_REQUEST = 'GROUP_CONVERSATION_REQUEST';
const groupConversationRequest = () => {
    return {
        type: GROUP_CONVERSATION_REQUEST
    }
}

export const GROUP_CONVERSATION_REQUEST_FAILURE = 'GROUP_CONVERSATION_REQUEST_FAILURE';
const groupConversationRequestFailure = (error) => {
    return {
        type: GROUP_CONVERSATION_REQUEST_FAILURE,
        error
    }
}

export const GROUP_CONVERSATION_REQUEST_SUCCESS = 'GROUP_CONVERSATION_REQUEST_SUCCESS';
const groupConversationRequestSuccess = (data) => {
    return {
        type: GROUP_CONVERSATION_REQUEST_SUCCESS,
        data
    }
}

export const fetchGroupConversation = (token, id) => {
    return async dispatch => {
        dispatch(groupConversationRequest());
        try {
            const response = await axios({
                method: 'get',
                url: `${getEndpoint('conversations', 'group')}/${id}`,
                headers: {'token': token}
            });
            if(response.status === 200) {
                dispatch(groupConversationRequestSuccess(response.data));
            }
        } catch(err) {
            dispatch(groupConversationRequestFailure(err));
        }
    }
}

export const SET_SOCKET = 'SET_SOCKET';
const setSocket = (socket) => {
    return {
        type: SET_SOCKET,
        socket
    }
}

export const socketSetUp = (socket, token) => {
    return async dispatch => {
        dispatch(setSocket(socket));
        socket.emit('status-update', {
            token,
            socket: socket.id,
            status: 'Active'
        });
    }
}

export const SET_CONVERSATION_STATUS = 'SET_CONVERSATION_STATUS';
const setConversationStatus = (uid, status) => {
    return {
        type: SET_CONVERSATION_STATUS,
        uid,
        status
    }
}

export const updateConversationStatus = (uid, status) => {
    return async dispatch => {
        dispatch(setConversationStatus(uid, status));
    }
}

export const TEAR_DOWN_SOCKET = 'TEAR_DOWN_SOCKET';
const tearDownSocket = (socket) => {
    return {
        type: TEAR_DOWN_SOCKET,
        socket
    }
}

export const tearSocketDown = (socket, token) => {
    return async dispatch => {
        socket.emit('status-update', {
            token,
            socket: socket.id,
            status: 'Offline'
        });
        socket.disconnect();
        dispatch(tearDownSocket(socket));
    }
}

export const VISIBILITY_CHANGE = 'VISIBILITY_CHANGE';
const visibilityChange = (status) => {
    return {
        type: VISIBILITY_CHANGE,
        status
    }
}

export const changeVisibilityStatus = (socket, token, status) => {
    return async dispatch => {
        socket.emit('status-update', {
            token,
            socket: socket.id,
            status
        });
        dispatch(visibilityChange(status))
    }
}

export const TOGGLE_CONVERSATION_LISTS = 'TOGGLE_CONVERSATION_LISTS';
export const toggleConversations = () => {
    return {
        type: TOGGLE_CONVERSATION_LISTS
    }
}

export const CLOSE_CONVERSATIONS_LIST = 'CLOSE_CONVERSATIONS_LIST';
export const closeConversations = () => {
    return {
        type: CLOSE_CONVERSATIONS_LIST
    }
}
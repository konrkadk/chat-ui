import React, { useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import CustomNavbar from '../components/CustomNavbar';
import Footer from '../components/Footer';
import { logout } from '../actions/auth';
import io from 'socket.io-client';
import { SimpleAssociatesList, SimpleGroupsList, ConversationsDrawer } from '../components/ConversationsList';
import ChatWindow from '../components/ChatWindow';
import { socketSetUp, fetchPrivateConversations, fetchGroupConversations,
         updateConversationStatus, tearSocketDown, toggleConversations, 
         closeConversations } from '../actions/chat';

const ChatWithConversations = ({associates, groups}) => {
    return (
      <div className="chat-container">
        <div className="private-conversations-list sm-hide">
          <SimpleAssociatesList associates={associates}/>
        </div>
        <div className="group-conversations-list sm-hide">
          <SimpleGroupsList groups={groups} />
        </div>
        <div className="chat-window">
                <ChatWindow />
        </div>
      </div>
    )
}

const ChatWithoutConversations = props => {
    return (
      <div className="chat-container-no-lists">
        <div className="chat-window">
            <ChatWindow />
        </div>
      </div>
    )
}

export default () => {
  const token = useSelector(state => state.authReducer.token);
  const socket = useSelector(state => state.chatReducer.socket);
  const conversations = useSelector(state => state.chatReducer.conversationLists);
  const dispatch = useDispatch();
  const handleSignOut = useCallback(() => {
      dispatch(logout())
      dispatch(tearSocketDown(socket, token))
    }, [dispatch, socket, token]
  );
  const handleConversationsToggle = useCallback(() => {
    dispatch(toggleConversations());
  }, [dispatch]);
  const handleConversationsClose = useCallback(() => {
    dispatch(closeConversations());
  }, [dispatch]);

  //temp

  const associates = [
    { firstname: 'Konrad', lastname: 'Gaca', status: 'active' },
    { firstname: 'Nick', lastname: 'Cave', status: 'away' },
    { firstname: 'Jack', lastname: 'White', status: 'active' },
    { firstname: 'Thomas', lastname: 'Bangalter', status: 'offline' },
    { firstname: 'Konrad', lastname: 'Gaca', status: 'active' },
    { firstname: 'Nick', lastname: 'Cave', status: 'away' },
    { firstname: 'Jack', lastname: 'White', status: 'active' },
    { firstname: 'Thomas', lastname: 'Bangalter', status: 'offline' },
    { firstname: 'Konrad', lastname: 'Gaca', status: 'active' },
    { firstname: 'Nick', lastname: 'Cave', status: 'away' },
    { firstname: 'Jack', lastname: 'White', status: 'active' },
    { firstname: 'Thomas', lastname: 'Bangalter', status: 'offline' },
  ]

  const groups = [
    { groupname: 'Directors' },
    { groupname: 'Developers' },
    { groupname: 'Producers' },
    { groupname: 'Managers' },
    { groupname: 'Software Qualiry Assurance' },
    { groupname: 'Directors' },
    { groupname: 'Developers' },
    { groupname: 'Producers' },
    { groupname: 'Managers' },
    { groupname: 'Software Qualiry Assurance' },
  ]

  //end of temp

  const socketSetup = () => {
    const socket = io('http://localhost:8888');
    socket.on('connect', () =>  {
      dispatch(socketSetUp(socket, token));
      dispatch(fetchPrivateConversations(socket, token));
    });
    socket.on('status-update123', data =>  {
      console.log(`Status Update: ${data}`);
      dispatch(updateConversationStatus(data.uid, data.status))
    });
    /*
    Keep all the sockets listeners in here
    1. on status change
    2. on messages (text, organizational, etc...)
    */
    // socket.on('some-msg' data => dispatch(someAction(data)));
  }

  useEffect(() => {
    if(!socket) {
      socketSetup(); //limits the socket setup only to the initial mount
    } else {
      dispatch(fetchPrivateConversations(socket, token)); //should be called in the private conversations list component
    }
    dispatch(fetchGroupConversations(token)); //should be called in the group conversations list component
    // eslint-disable-next-line
  }, []);

  const chat = conversations ? <ChatWithConversations
                                associates={associates}
                                groups={groups}/> : <ChatWithoutConversations />

  return (
    <div className="container">
      <div className="navbar">
        <CustomNavbar onSignOut={handleSignOut} onConversationsToggle={handleConversationsToggle}/>
      </div>
      <ConversationsDrawer toggle={conversations} closeHandler={handleConversationsClose} associates={associates} groups={groups}/>
      {chat}
    <Footer />
    </div>
  );
}
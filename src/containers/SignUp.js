import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Navbar from '../components/Navbar';
import SignUpLayout from '../components/SignUpLayout';
import Footer from '../components/Footer';
import { signup } from '../actions/auth';

export default () => {
    const loading = useSelector(state => state.authReducer.loading); //as in signing up
    const error = useSelector(state => state.authReducer.error);
    const dispatch = useDispatch();

    const handleSignUp = useCallback(
        (data) => dispatch(signup(data)),
        [dispatch]
    );

    return (
        <div className="container">
            <Navbar />
            {/* Add a simple presentational component that will hold the form
                It will be responsible for the following:
                1. Managing the css grid magic (should have a css class similar to the SignInForm component )
                2. Displaying a nice paper background with a page title at the top
                3. Holding the form components (logic and presentation)
                4. Passing the loading and handleSignUp props down to the above mentioned components
            */}
            <SignUpLayout loading={loading} error={error} handleSignUp={handleSignUp}/>
            <Footer />
        </div>
    );
}
import React, { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import SignInForm from '../components/SignInForm';
import { authenticate } from '../actions/auth';

const useStyles = makeStyles(theme => ({
    root: {
      padding: theme.spacing(2, 4),
    },
    link: {
     textAlign: 'center',
    },
    linkText: {
        textDecoration: 'none'
    },
    title: {
        padding: theme.spacing(2, 0)
    }
}));

const SignUpLink = () => {
    const classes = useStyles();
    return (
        <div className={classes.link}>
            <Link to='/signup' color="primary" className={classes.linkText}>
                <Typography variant="body1" color="textPrimary">
                    Don't have an account?
                </Typography>
            </Link>
        </div>
    )
}

export default () => {
    const classes = useStyles();
    const loading = useSelector(state => state.authReducer.loading);
    const error = useSelector(state => state.authReducer.error);
    const dispatch = useDispatch();

    const handleSignIn = useCallback(
        (email, password) => dispatch(authenticate(email, password)),
        [dispatch]
    );

    return (
        <div className="container">
            <Navbar />
            <div className="sign-in-content">
                <Paper className={classes.root}>
                    <Typography variant="h5" component="h3" className={classes.title}>
                        Sign In
                    </Typography>
                    <SignInForm error={error} loading={loading} onSubmit={handleSignIn} />
                    <SignUpLink />
                </Paper>
            </div>
            <Footer />
        </div>
    );
}
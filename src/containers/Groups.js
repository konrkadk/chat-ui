import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import CustomNavbar from '../components/CustomNavbar';
import Footer from '../components/Footer';
import { logout } from '../actions/auth';

export default () => {
  const dispatch = useDispatch();
  const handleSignOut = useCallback(
    () => dispatch(logout()), [dispatch]
  );
  
  return (
    <div className="container">
      <div className="navbar">
        <CustomNavbar onSignOut={handleSignOut}/>
      </div>
      <div className="main-content">
        <h3>This is the placeholder for the Groups</h3>
      </div>
    <Footer />
    </div>
  );
}
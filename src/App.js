import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Router, Route, Switch } from 'react-router-dom';
import PageVisibility from 'react-page-visibility';
import SignIn from './containers/SignIn';
import SingUp from './containers/SignUp';
import Chat from './containers/Chat';
import Associates from './containers/Associates';
import Groups from './containers/Groups';
import Settings from './containers/Settings';
import history from './utils/history';
import ProtectedRoute from './components/ProtectedRoute';
import './App.css';
import './Layout.css';
import './Styles.css';
import { changeVisibilityStatus } from './actions/chat';

export default () => {
  const authenticated = useSelector(state => state.authReducer.authenticated);
  const token = useSelector(state => state.authReducer.token);
  const socket = useSelector(state => state.chatReducer.socket);
  const dispatch = useDispatch();
  
  const handleVisibilityChange = (visible) => {
    if(authenticated) {
      const status = visible ? 'Active' : 'Away';
      dispatch(changeVisibilityStatus(socket, token, status));
    }
  };

  return (
    <PageVisibility onChange={handleVisibilityChange}>
      <Router history={history}>
        <Switch>
          <Route exact path='/' component={props => <SignIn {...props} />} />
          <Route path='/signup' component={props => <SingUp {...props} />} />
          <ProtectedRoute authenticated={authenticated} path='/chat' component={props => <Chat {...props} />} />
          <ProtectedRoute authenticated={authenticated} path='/associates' component={props => <Associates {...props} />} />
          <ProtectedRoute authenticated={authenticated} path='/groups' component={props => <Groups {...props} />} />
          <ProtectedRoute authenticated={authenticated} path='/settings' component={props => <Settings {...props} />} />
        </Switch>
      </Router>
    </PageVisibility>
  );
}


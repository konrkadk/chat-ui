import config from '../config/config-dev.json';

const backendUrl = `http://${config.baseUrl}`;

export const getEndpoint = (route, endpoint) => {
    return `${backendUrl}${config.endpoints[route][endpoint]}`;
}
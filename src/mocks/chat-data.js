export const messages = [
    {
        date: new Date(),
        content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore`,
        own: false
    },
    {
        date: new Date(),
        content: `This is a response to the previous message.
                This is a response to the previous message.
                This is a response to the previous message.
                This is a response to the previous message.`,
        own: true
    },
]
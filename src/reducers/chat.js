import { PRIVATE_CONVERSATIONS_REQUEST, PRIVATE_CONVERSATIONS_REQUEST_FAILURE, PRIVATE_CONVERSATIONS_REQUEST_SUCCESS,
    GROUP_CONVERSATIONS_REQUEST, GROUP_CONVERSATIONS_REQUEST_FAILURE, GROUP_CONVERSATIONS_REQUEST_SUCCESS,
    PRIVATE_CONVERSATION_REQUEST, PRIVATE_CONVERSATION_REQUEST_FAILURE, PRIVATE_CONVERSATION_REQUEST_SUCCESS,
    GROUP_CONVERSATION_REQUEST, GROUP_CONVERSATION_REQUEST_FAILURE, GROUP_CONVERSATION_REQUEST_SUCCESS,
    SET_SOCKET, SET_CONVERSATION_STATUS, TEAR_DOWN_SOCKET, VISIBILITY_CHANGE, TOGGLE_CONVERSATION_LISTS,
    CLOSE_CONVERSATIONS_LIST } from '../actions/chat';

const init = {
    socket:null,
    privateConversations:[],
    groupConversations:[],
    activeConversation:{},
    isFetchingMessages:false,
    isFetchingPrivate:false,
    isFetchingGroup:false,
    isFetchingConversation:false,
    isTyping: false,
    messageContent: '',
    privateConversationsError:'',
    groupConversationsError:'',
    selectedConversationError:'',
    visibility:'',
    conversationLists: true,
}

export default (state = init, action) => {
    switch(action.type) {
        case PRIVATE_CONVERSATIONS_REQUEST:
            return {
                ...state,
                isFetchingPrivate: true
            }
        case PRIVATE_CONVERSATIONS_REQUEST_FAILURE:
            return {
                ...state,
                privateConversationsError: action.error,
                isFetchingPrivate: false
            }
        case PRIVATE_CONVERSATIONS_REQUEST_SUCCESS:
            return {
                ...state,
                privateConversations:action.data.conversations,
                isFetchingPrivate: false
            }
        case GROUP_CONVERSATIONS_REQUEST:
            return {
                ...state,
                isFetchingGroup: true
            }
        case GROUP_CONVERSATIONS_REQUEST_FAILURE:
            return {
                ...state,
                groupConversationsError: action.error,
                isFetchingGroup: false
            }
        case GROUP_CONVERSATIONS_REQUEST_SUCCESS:
            return {
                ...state,
                groupConversations:action.data.conversations,
                isFetchingGroup: false
            }
        case PRIVATE_CONVERSATION_REQUEST:
            return {
                ...state,
                isFetchingConversation: true
            }
        case PRIVATE_CONVERSATION_REQUEST_FAILURE:
            return {
                ...state,
                isFetchingConversation: false,
                selectedConversationError: action.error
            }
        case PRIVATE_CONVERSATION_REQUEST_SUCCESS:
            return {
                ...state,
                isFetchingConversation: false,
                activeConversation: action.data.conversation
            }
        case GROUP_CONVERSATION_REQUEST:
            return {
                ...state,
                isFetchingConversation: true
            }
        case GROUP_CONVERSATION_REQUEST_FAILURE:
            return {
                ...state,
                isFetchingConversation: false,
                selectedConversationError: action.error
            }
        case GROUP_CONVERSATION_REQUEST_SUCCESS:
            return {
                ...state,
                isFetchingConversation: false,
                activeConversation: action.data.conversation
            }
        case SET_SOCKET:
            return {
                ...state,
                socket: action.socket
            }
        case SET_CONVERSATION_STATUS:
            const newPrivateConversations = state.privateConversations;
            const index = newPrivateConversations.findIndex(conversation => {
                return conversation.participants.includes(action.uid);
            });
            if(index >= 0)
                newPrivateConversations[index].status = action.status;
            return {
                ...state,
                privateConversations: newPrivateConversations
            }
        case TEAR_DOWN_SOCKET:
            return {
                ...state,
                socket: null
            }
        case VISIBILITY_CHANGE:
            return {
                ...state,
                visibility: action.status
            }
        case TOGGLE_CONVERSATION_LISTS:
            return {
                ...state,
                conversationLists: !state.conversationLists
            }
        case CLOSE_CONVERSATIONS_LIST:
            return {
                ...state,
                conversationLists: false
            }
        default:
            return state;
    }
}
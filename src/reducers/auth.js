import * as AUTH from '../actions/auth';

const init = {
    authenticated: true,
    error: '',
    token: '',
    loading: false
}

export default (state = init, action) => {
    switch(action.type) {
        case AUTH.AUTH_REQUEST:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case AUTH.AUTH_REQUEST_SUCCESS:
            return {
                ...state,
                authenticated: true,
                token: action.data.token,
                loading: false
            }
        case AUTH.AUTH_REQUEST_FAILURE:
            return {
                ...state,
                authenticated: false,
                error: action.msg,
                loading: false
            }
        case AUTH.LOGOUT_REQUEST:
            return {
                ...state,
                authenticated: false,
                token: ''
            }
        case AUTH.SIGN_UP_REQUEST:
            return {
                ...state,
                loading: true,
                error: ''
            }
        case AUTH.SIGN_UP_SUCCESS:
            return {
                ...state,
                authenticated: true,
                token: action.data.token,
                loading: false
            }
        case AUTH.SIGN_UP_FAILURE:
            return {
                ...state,
                authenticated: false,
                loading: false,
                error: action.msg
            }
        default:
            return state;
    }
}
import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { withStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MessageIcon from '@material-ui/icons/Message';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import SettingsIcon from '@material-ui/icons/Settings';
import MoreIcon from '@material-ui/icons/MoreVert';
import Tooltip from '@material-ui/core/Tooltip';

const styles = theme => ({
  root: {
    width: '100%',
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'block',
    },
  },
    link: {
        textDecoration: 'none',
        '&:focus': {
            color: 'white'
        },
        '&:hover': {
            color: 'white'
        },
        '&:visited': {
            color: 'white'
        },
        '&:link': {
            color: 'white'
        },
        '&:active': {
            color: 'white'
        },
        color: 'white'
    },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
});

class CustomNavbar extends React.Component {
  state = {
    anchorEl: null,
    mobileMoreAnchorEl: null,
  };

  handleMobileMenuOpen = event => {
    this.setState({ mobileMoreAnchorEl: event.currentTarget });
  };

  handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: null });
  };

  handleSignOut = () => {
    this.props.onSignOut();
  }

  handleConversationsToggle = () => {
      this.props.onConversationsToggle();
  }

  render() {
    const { anchorEl, mobileMoreAnchorEl } = this.state;
    const { classes } = this.props;
    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

      const renderMobileMenu = (
          <Menu
              anchorEl={mobileMoreAnchorEl}
              anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
              transformOrigin={{ vertical: 'top', horizontal: 'right' }}
              open={isMobileMenuOpen}
              onClose={this.handleMobileMenuClose}>
              <Link to='/chat' className={classes.link}>
                <MenuItem>
                    <IconButton color="inherit">
                        <MessageIcon />
                    </IconButton>
                    <p>Chat</p>
                </MenuItem>
              </Link>
              <Link to='/associates' className={classes.link}>
                <MenuItem>
                    <IconButton color="inherit">
                        <PersonAddIcon />
                    </IconButton>
                    <p>Associates</p>
                </MenuItem>
              </Link>
              <Link to='/groups' className={classes.link}>
                <MenuItem>
                    <IconButton color="inherit">
                        <GroupAddIcon />
                    </IconButton>
                    <p>Groups</p>
                </MenuItem>
              </Link>
              <Link to='/settings' className={classes.link}>
                <MenuItem>
                    <IconButton color="inherit">
                        <SettingsIcon />
                    </IconButton>
                    <p>Settings</p>
                </MenuItem>
              </Link>
              <MenuItem onClick={this.handleSignOut}>
                  <IconButton color="inherit">
                      <AccountCircle />
                  </IconButton>
                  <p>Sign Out</p>
              </MenuItem>
          </Menu>
      );

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    {/* <Tooltip title="Conversations"> had to remove it due to a material ui bug that adds 17px padding to the body element due to the conflict between the drawer and the tooltip*/}
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Open drawer" onClick={this.handleConversationsToggle}>
                            <MenuIcon />
                        </IconButton>
                    {/* </Tooltip> */}
                    <Typography variant="h5" color="inherit">
                        Chatch
                    </Typography>
                    <div className={classes.grow} />
                    <div className={classes.sectionDesktop}>
                        <Link to='/chat' className={classes.link}>
                            <Tooltip title="Chat">
                                <IconButton color="inherit">
                                    <MessageIcon />
                                </IconButton>
                            </Tooltip>
                        </Link>
                        <Link to='/associates' className={classes.link}>
                            <Tooltip title="Associates">
                                <IconButton color="inherit">
                                    <PersonAddIcon />
                                </IconButton>
                            </Tooltip>
                        </Link>
                        <Link to='/groups' className={classes.link}>
                            <Tooltip title="Groups">
                                <IconButton color="inherit">
                                    <GroupAddIcon />
                                </IconButton>
                            </Tooltip>
                        </Link>
                        <Link to='/settings' className={classes.link}>
                            <Tooltip title="Settings">
                                <IconButton color="inherit">
                                    <SettingsIcon />
                                </IconButton>
                            </Tooltip>
                        </Link>
                        <Tooltip title="Sign Out">
                            <IconButton
                                aria-owns={isMenuOpen ? 'material-appbar' : undefined}
                                onClick={this.handleSignOut}
                                color="inherit"
                            >
                                <AccountCircle />
                            </IconButton>
                        </Tooltip>
                    </div>
                    <div className={classes.sectionMobile}>
                        <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit">
                            <MoreIcon />
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
            {renderMobileMenu}
        </div>
    );
  }
}

CustomNavbar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CustomNavbar);

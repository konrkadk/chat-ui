import React from "react";
import TextField from '@material-ui/core/TextField';
import { SubmitBox, LoadingBox } from './SigningFormsHelpers';
import { withFormik } from 'formik';
import * as yup from 'yup';
import SignInError from './SignInError';

const schema = yup.object().shape({
    email: yup.string().email('Must be a valid email addess').required('Required'),
    firstname: yup.string().min(2, 'First name has to longer').required('Required'),
    lastname: yup.string().min(2, 'Last name has to be longer').required('Required'),
    password: yup.string().min(6, 'Password has to be longer').required('Required')
});

const SignUpForm = p => {    
    const touched = p.touched.email || p.touched.firstname || p.touched.lastname || p.touched.password;
    const errors = touched ? <SignInError error={p.error}/> : null;
    const submitBox = p.loading ? <LoadingBox /> : <SubmitBox touched={p.touched} handleReset={p.handleReset}/>

    return (
        <form onSubmit={p.handleSubmit}>
            <TextField
                type="email"
                name="email"
                label="Email"
                helperText={p.errors.email && p.touched.email ? p.errors.email : ' '}
                margin={'normal'}
                variant="outlined"
                fullWidth={true}
                onChange={p.handleChange}
                onBlur={p.handleBlur}
                value={p.values.email}
                error={p.errors.email && p.touched.email}
                autoComplete="off"
            />
            <TextField
                type="text"
                name="firstname"
                label="First Name"
                helperText={p.errors.firstname && p.touched.firstname ? p.errors.firstname : ' '}
                margin={'normal'}
                variant="outlined"
                fullWidth={true}
                onChange={p.handleChange}
                onBlur={p.handleBlur}
                value={p.values.firstname}
                error={p.errors.firstname && p.touched.firstname}
                autoComplete="off"
            />
            <TextField
                type="text"
                name="lastname"
                label="Last Name"
                helperText={p.errors.lastname && p.touched.lastname ? p.errors.lastname : ' '}
                margin={'normal'}
                variant="outlined"
                fullWidth={true}
                onChange={p.handleChange}
                onBlur={p.handleBlur}
                value={p.values.lastname}
                error={p.errors.lastname && p.touched.lastname}
                autoComplete="off"
            />
            <TextField
                type="password"
                name="password"
                label="Password"
                helperText={p.errors.password && p.touched.password ? p.errors.password : ' '}
                margin={'normal'}
                variant="outlined"
                fullWidth={true}
                onChange={p.handleChange}
                onBlur={p.handleBlur}
                value={p.values.password}
                error={p.errors.password && p.touched.password}
                autoComplete="off"
            />
            {submitBox}
            {errors}
        </form>
    )
}

export default withFormik({
    mapPropsToValues: () => ({ 
        email: '',
        firstname: '',
        lastname: '',
        password: '',
     }),
     handleSubmit: (values, {props, setSubmitting}) => {
      setSubmitting(false);
      props.handleSignUp(values);
    },
    validationSchema: schema,
    displayName: 'EmailInputForm',
})(SignUpForm);
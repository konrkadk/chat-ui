import React from "react";

export default ({error}) => {
    let css, text;
    if(error) {
        css = 'error';
        text = error;
    } else {
        css = 'hidden';
        text = 'x';
    }
    return (
        <div className={css}>
            {text}
        </div>
    )
};

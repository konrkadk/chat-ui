import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Avatar from "@material-ui/core/Avatar";
import IconButton from '@material-ui/core/IconButton';
import MoreIcon from '@material-ui/icons/MoreVert';
import Toolbar from '@material-ui/core/Toolbar';
import TextField from "@material-ui/core/TextField";
import { UserStatusLight } from './ConversationsList';
import { messages } from '../mocks/chat-data';

const useStyles = makeStyles(theme => ({
    root: {
        height: "100%"
    },
    headerDiv: {
        height: "100%",
        marginBottom: '4px'
    },
    paperMessagesBox: {
        height: "100%",
        marginBottom: '4px'
    },
    paperMessagesInput: {
        // height: "100%",
        marginTop: '4px'
    },
    title: {
        padding: theme.spacing(2, 2, 2, 2),
    },
    toolbar: {
        alignItems: 'center',
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    grow: {
        flexGrow: 1
    },
    msginput: {
        // height: '4em',
    },
    msgbox: {
        height: '100%',
        padding: theme.spacing(1.4, 1.4),
        alignItems: 'right'
    },
    msgbubble: {
        borderRadius: '8px',
        backgroundColor: '#3949ab',
        opacity: 0.7,
        padding: theme.spacing(1.2, 1.2),
        margin: '2px',
        width: '60%',
    },
    msgdate: {
        fontSize: '0.8em',
        opacity: 0.7
    },
    msgcontent: {

    },
    ownmsg: {
        float: 'right',
        backgroundColor: '#6a39ab'
    },
}));

const ActiveConversationAvatar = ({status, firstname, lastname, avatar}) => {
    const classes = useStyles();
    const initials = firstname[0] + lastname[0];
    return (
        <div className={classes.headerDiv}>
        <Paper className={classes.paperHeader}>
        <Toolbar className={classes.toolbar}>
            <UserStatusLight edge="start" status={status} />
            <Avatar edge="start">{
                <Typography variant="h6" className={classes.title}>
                {initials}
                </Typography>
            }</Avatar>
            <Typography variant="h6" className={classes.title}>
                {`${firstname} ${lastname}`}
            </Typography>
            <div className={classes.grow} />
            <IconButton aria-label="display more actions" edge="end" color="inherit">
                <MoreIcon />
            </IconButton>
        </Toolbar>
        </Paper>
        </div>
    )
}

const ActiveGroupAvatar = ({status, name, avatar}) => {
    // const classes = useStyles();
}

const MessagesBox = ({messages}) => {
    const classes = useStyles();
    const bubbles = messages.map(message => {
        return <MessageBubble date={message.date} content={message.content} own={message.own}/>
    });
    return (
        <Paper className={classes.paperMessagesBox}>
            <div className={classes.msgbox}>{bubbles}</div>
        </Paper>
    )
}

const MessageInputBox = () => {
    const classes = useStyles();
    return (
        <Paper className={classes.paperMessagesInput}>
            <div className={classes.msginput}>
            <form className={classes.root} noValidate autoComplete="off">
      <div>
        <TextField id="outlined-basic" variant="outlined" 
          fullWidth={true}
          multiline={true}/>
      </div>
    </form>
            </div>
        </Paper>
    )
}

const MessageBubble = ({date, content, own}) => {
    const classes = useStyles();
    const ownership = own ? classes.ownmsg : '';
    return (
        <div className={`${classes.msgbubble} ${ownership}`}>
            <div className={classes.msgdate}>
                {date.toLocaleString()}
            </div>
            <MessageContent content={content}/>
        </div>
    )
}

const MessageContent = ({content}) => {
    const classes = useStyles();
    return (
        <div className={classes.msgcontent}>{content}</div>
    )
}

export default props => {
    const msgs = messages //temp
    return (
        <>
            <div className="messages-header">
                <ActiveConversationAvatar status={'active'} firstname={'Konrad'} lastname={'Gaca'} avatar={null}/>
            </div>
            <div className="messages-box">
                <MessagesBox messages={msgs}/>
            </div>
            <div className="messages-input">
                <MessageInputBox/>
            </div>
        </>
    )
}
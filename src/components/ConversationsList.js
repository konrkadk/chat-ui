import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Group from "@material-ui/icons/Group";
import { Drawer, Divider } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        overflowY: "auto",
        margin: 0,
        padding: 0,
        listStyle: "none",
        height: "100%",
        '&::-webkit-scrollbar': {
            width: '0.4em'
        },
        '&::-webkit-scrollbar-track': {
            boxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)',
            webkitBoxShadow: 'inset 0 0 6px rgba(0,0,0,0.00)'
        },
        '&::-webkit-scrollbar-thumb': {
            backgroundColor: 'rgba(0,0,0,.1)',
            outline: '1px solid slategrey'
        }
    },
    statusLight: {
        paddingLeft: 2,
        paddingRight: 5
    },
    active: {
        color: "green"
    },
    away: {
        color: "yellow"
    },
    offline: {
        color: "red"
    },
    title: {
        padding: theme.spacing(2, 2)
    },
}));

export const UserStatusLight = ({status}) => {
    const classes = useStyles();
    const color = classes[`${status}`];
    return (
        <p className={`${classes.statusLight} ${color}`}>
            {"\u26AB"}
        </p>
    )
}

export const AssociateElement = ({associate}) => {
    return (
        <ListItem button>
            <UserStatusLight status={associate.status} />
            <ListItemAvatar>
                <Avatar>
                    <AccountCircle />
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary={`${associate.firstname} ${associate.lastname}`} />
        </ListItem>
    )
}

const GroupElement = ({group}) => {
    const gname = 'Groupname';
    return (
        <ListItem button>
            <ListItemAvatar>
                <Avatar>
                    <Group />
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary={gname} />
        </ListItem>
    )
}

export const SimpleAssociatesList = ({associates}) => {
    const classes = useStyles();
    const items = associates.map(associate => {
        return <AssociateElement associate={associate}/>
    });
    return (
        <Paper className={classes.root}>
            <Typography variant="b1" component="h3" className={classes.title}>
                Conversations
            </Typography>
            <List>
                {items}
            </List>
        </Paper>
    )
}

export const SimpleGroupsList = ({groups}) => {
    const classes = useStyles();
    const items = groups.map(group => {
        return <GroupElement group={group}/>
    });
    return (
        <Paper className={classes.root}>
            <Typography variant="b1" component="h3" className={classes.title}>
                Groups
            </Typography>
                <List>
                    {items}
                </List>
        </Paper>
    )
}

const DrawerAssociatesList = ({associates}) => {
    const classes = useStyles();
    const items = associates.map(associate => {
        return <AssociateElement associate={associate}/>
    });
    return (
        <>
        <Typography variant="b1" component="h3" className={classes.title}>
                Conversations
        </Typography>
        <List>
            {items}
        </List>
        </>
    )
}

const DrawerGroupsList = ({groups}) => {
    const classes = useStyles();
    const items = groups.map(group => {
        return <GroupElement group={group}/>
    });
    return (
        <>
            <Typography variant="b1" component="h3" className={classes.title}>
                Groups
            </Typography>
                <List>
                    {items}
                </List>
        </>
    )
}

export const ConversationsDrawer = ({toggle, closeHandler, associates, groups}) => {
    const classes = useStyles();
    return (
        <Drawer open={toggle} onClose={closeHandler} className="conversations-drawer">
            <div role="presentation" onClick={closeHandler} className={classes.root}>
                <DrawerAssociatesList associates={associates}/>
                <Divider />
                <DrawerGroupsList groups={groups}/>
            </div>
        </Drawer>
    )
}
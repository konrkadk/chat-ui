import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fab from '@material-ui/core/Fab';
import SendIcon from '@material-ui/icons/Send';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';

const useStyles = makeStyles(theme => ({
    buttonField: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
}));

export const SubmitBox = p => {
    const classes = useStyles();
    return (
        <div className="sign-in-container">
            <div className={classes.buttonField}>
                <Fab color="primary"
                    variant="extended"
                    disabled={!(p.touched.email || p.touched.firstname || p.touched.lastname || p.touched.password)}
                    onClick={p.handleReset}>
                    <RotateLeftIcon className={classes.extendedIcon} />
                    Reset
            </Fab>
                <Fab color="secondary"
                    variant="extended"
                    type="submit">
                    <SendIcon className={classes.extendedIcon} />
                    Submit
            </Fab>
            </div>
        </div>   
    )
}

export const LoadingBox = p => {
    return (
        <div className="sign-in-container">
            <CircularProgress size={32}/>
        </div>
    )
}
import React from "react";
import TextField from '@material-ui/core/TextField';
import SignInError from "./SignInError";
import { withFormik } from 'formik';
import * as yup from 'yup';
import { SubmitBox, LoadingBox } from './SigningFormsHelpers';

const schema = yup.object().shape({
    email: yup.string().email('Must be a valid email addess').required('Required'),
    password: yup.string().required('Required')
});

const SignInForm = p => {
    const touched = p.touched.email || p.touched.password;
    const errors = touched ? <SignInError error={p.error}/> : null;
    const submitBox = p.loading ? <LoadingBox /> : <SubmitBox touched={p.touched} handleReset={p.handleReset}/>

    return (
        <form onSubmit={p.handleSubmit}>
            <TextField
                type="email"
                name="email"
                label="Email"
                helperText={p.errors.email && p.touched.email ? p.errors.email : ' '}
                margin={'normal'}
                variant="outlined"
                fullWidth={true}
                onChange={p.handleChange}
                onBlur={p.handleBlur}
                value={p.values.email}
                error={p.errors.email && p.touched.email}
                autoComplete="off"
            />
            <TextField
                type="password"
                name="password"
                label="Password"
                helperText={p.errors.password && p.touched.password ? p.errors.password : ' '}
                margin={'normal'}
                variant="outlined"
                fullWidth={true}
                onChange={p.handleChange}
                onBlur={p.handleBlur}
                value={p.values.password}
                error={p.errors.password && p.touched.password}
                autoComplete="off"
            />
            {submitBox}
            {errors}
        </form>
    )
}

export default withFormik({
    mapPropsToValues: () => ({ 
        email: '',
        password: '',
     }),
     handleSubmit: (values, {props, setSubmitting}) => {
      setSubmitting(false);
      props.onSubmit(values.email, values.password);
    },
    validationSchema: schema,
    displayName: 'SignInForm',
})(SignInForm);


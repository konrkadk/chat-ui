import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import SignUpForm from './SignUpForm';

const useStyles = makeStyles(theme => ({
    root: {
      padding: theme.spacing(4, 4),
    },
    title: {
        padding: theme.spacing(2, 0)
    }
}));

export default ({loading, error, handleSignUp}) => {
    const classes = useStyles();
    return (
        <div className="sign-up-content">
            <Paper className={classes.root}>
                <Typography variant="h5" component="h3" className={classes.title}>
                    Sign Up
                </Typography>
                <SignUpForm loading={loading} error={error} handleSignUp={handleSignUp}/>
            </Paper>
        </div>
    )
};
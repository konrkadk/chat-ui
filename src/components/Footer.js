import React from "react";

export default () => {
    return (
        <div className="footer">
            <div className="upper">
                ~Konrad 2019
            </div>
            <div className="lower"></div>
        </div>
    )
};


import React from "react";
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';

export default ({loading, onSubmit}) => {
    const content = loading ? <CircularProgress size={32}/> : <Button variant="outlined" color="secondary" disabled={false} onClick={onSubmit}>Sign In</Button>;
    return (
        <div className="sign-in-container">
            {content}
        </div>
    )
};
